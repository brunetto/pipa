

  /*****************************************************************************
   *
   * Event listeners for UI elements
   *
   ****************************************************************************/

  window.onload = function(){
       document.getElementById("udc").focus();
  };

  document.getElementById('udc').addEventListener('change', function() {
    document.getElementById("pkg").focus();

  });

  document.getElementById('pkg').addEventListener('change', function() {
    document.getElementById("udc").value = '';
    document.getElementById("pkg").value = '';
    document.getElementById("udc").focus();
  });
